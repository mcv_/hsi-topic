"""
Created by Murat Can VARER 19.12.20
"""

import os

import yaml


class DotDict(dict):
    """dict class that allows you to access values by dot notation"""

    def __init__(self, arg):
        for k in arg.keys():
            if (type(arg[k]) is dict):
                self[k] = DotDict(arg[k])
            else:
                self[k] = arg[k]

    def __getattr__(self, attr):
        return self.get(attr, None)

    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


class Config(DotDict):
    def __init__(self):
        self.readPath()
        try:
            with open(self.path, 'r') as ymlfile:
                super().__init__(yaml.safe_load(ymlfile))
        except:
            raise FileNotFoundError("Check the config.yml")

    def readPath(self):
        if os.path.isfile('config.yml'):
            self.path = 'config.yml'

