"""
Created by Murat Can VARER 20.12.20
"""

from keras.optimizers import Adam, SGD, RMSprop, Adagrad, Adamax, Nadam


def getOptimizationAlgorithm(cfg):
    if cfg.model.optimizer == "sgd":
        optimizer = SGD(lr=eval(cfg.model.lr), decay=1e-6)
    elif cfg.model.optimizer == "adam":
        optimizer = Adam(lr=eval(cfg.model.lr), decay=1e-06)
    elif cfg.model.optimizer == "adagrad":
        optimizer = Adagrad(lr=eval(cfg.model.lr))
    elif cfg.model.optimizer == "rmsprop":
        optimizer = RMSprop(lr=eval(cfg.model.lr), rho=0.9)
    elif cfg.model.optimizer == "adamax":
        optimizer = Adamax(lr=eval(cfg.model.lr), beta_1=0.9, beta_2=0.999)
    elif cfg.model.optimizer == "nadam":
        optimizer = Nadam(lr=eval(cfg.model.lr), beta_1=0.9, beta_2=0.999)

    return optimizer
