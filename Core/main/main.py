"""
Created by Murat Can VARER 18.12.20
"""
import shutil
import spectral
import os

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"
import tensorflow as tf

from sklearn.metrics import classification_report, cohen_kappa_score, confusion_matrix, accuracy_score
from keras.utils import np_utils, plot_model
# from keras.utils import plot_model

from Core.model.preprocessing import *
from Core.model.model import *
# from Core.model.brain_model import *
from Core.controller.config import Config
import Core.controller.helper as helper

helper.gpuMemLimiter()
config = Config()
helper.fileCheck()


class Execute:
    def __init__(self):
        self.config = config
        self.datasetName = self.config.dataset.name
        self.windowSize = self.config.preproces.windowSize
        self.testRatio = self.config.preproces.testRatio
        self.classes = self.config.dataset.classes
        self.batchSize = self.config.model.batchSize
        self.epochs = self.config.model.epochs
        self.inferenceId = helper.do_uuid()

        self.initData()
        self.splitDataset()
        self.reshapeDataset()
        self.createModel()
        self.startTrain()
        self.evaluateModel()
        if self.config.visualization:
            self.calculatePredictedImage()
        self.saveResult()
        print("Training & Evaluate has finished.")

    def initData(self):
        self.pureX, self.pureY = helper.loadData(config=self.config)
        self.K = 32 if self.datasetName == 'IP' else 16
        self.pca_X, pca = applyPCA(self.pureX, numComponents=self.K)
        self.X, self.y = createImageCubes(self.pca_X, self.pureY,
                                          windowSize=self.windowSize)

        self.inputShape = (self.windowSize, self.windowSize, self.K, 1)

    def splitDataset(self):
        self.Xtrain, self.Xtest, self.ytrain, self.ytest = splitTrainTestSet(self.X, self.y,
                                                                             self.testRatio)

    def reshapeDataset(self):
        self.Xtrain = self.Xtrain.reshape(-1, self.windowSize, self.windowSize, self.K, 1)
        self.Xtest = self.Xtest.reshape(-1, self.windowSize, self.windowSize, self.K, 1)
        self.ytrain = np_utils.to_categorical(self.ytrain)
        self.ytest = np_utils.to_categorical(self.ytest)

    def createModel(self):
        self.model = build_model(input_shape=self.inputShape, output_channels=self.classes,
                                 config=self.config)
        # self.model.summary()
        # plot_model(self.model, to_file='model.png')

    def startTrain(self):
        if tf.config.list_physical_devices('GPU'):
            self.model.fit(self.Xtrain, y=self.ytrain,
                           batch_size=self.batchSize, epochs=self.epochs)

            if self.config.saveModel:
                saveName = str(self.inferenceId) + ".h5"
                savePath = os.path.join(self.config.savingModelPath, self.datasetName)
                self.model.save(os.path.join(savePath, saveName))
                shutil.copy2('config.yml', savePath)
                os.rename(os.path.join(savePath, "config.yml"),
                          os.path.join(savePath, str(self.inferenceId) + ".yml"))
        else:
            raise PermissionError("Not using GPU!")

    def evaluateModel(self):
        Y_pred = self.model.predict(self.Xtest)
        Y_pred = np.argmax(Y_pred, axis=1)

        classification, confusion, testLoos, testAcc, oa, each_acc, aa, kappa = \
            helper.reports(self.Xtest, self.ytest, Y_pred, self.datasetName, self.model)

        self.kappa = kappa
        self.overallAcc = oa
        self.accuracyScore = aa

    def calculatePredictedImage(self):
        height = self.pureY.shape[0]
        width = self.pureY.shape[1]
        patchSize = self.windowSize
        X = padWithZeros(self.pca_X, patchSize // 2)
        outputs = np.zeros((height, width))
        for i in range(height):
            for j in range(width):
                target = int(self.pureY[i, j])
                if target == 0:
                    continue
                else:
                    image_patch = helper.Patch(X, i, j, patchSize)
                    X_test_image = image_patch.reshape(1, image_patch.shape[0], image_patch.shape[1],
                                                       image_patch.shape[2], 1).astype('float32')
                    predictionImage = (self.model.predict(X_test_image))
                    predictionImage = np.argmax(predictionImage, axis=1)
                    outputs[i][j] = predictionImage + 1

        groundSaveName = str(self.datasetName) + "_ground_" + ".png"
        if not os.path.isfile(os.path.join(self.config.savingPredictedImage,
                                           self.datasetName, groundSaveName)):
            spectral.save_rgb(data=self.pureY, filename=os.path.join(self.config.savingPredictedImage,
                                                                     self.datasetName, groundSaveName),
                              format="png", colors=spectral.spy_colors)

        predictedSaveName = str(self.inferenceId) + "_predicted_" + ".png"
        spectral.save_rgb(data=outputs.astype(int), filename=os.path.join(self.config.savingPredictedImage,
                                                                          self.datasetName, predictedSaveName),
                          format="png", colors=spectral.spy_colors)

    def saveResult(self):
        helper.saveResultCSV(self.config, self.datasetName, self.inferenceId,
                             self.kappa, self.overallAcc, self.accuracyScore)
        print("has been saved the predictions as the csv file.")


if __name__ == "__main__":
    Execute()
