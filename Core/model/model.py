"""
Created by Murat Can VARER 14.01.20
"""

import keras.backend as K
from tensorflow.python.keras.layers import Conv3D, Activation, Add, UpSampling3D, Lambda, Dense, Conv2D
from tensorflow.python.keras.layers import Input, Reshape, Flatten, Dropout
from keras.optimizers import Adam, SGD, RMSprop, Adagrad, Adamax, Nadam
from tensorflow.python.keras.models import Model

from Core.model.group_norm import GroupNormalization
import tensorflow as tf
import Core.controller.optimizations as opti


def green_block(inp, filters, data_format='channels_last', name=None):
    inp_res = Conv3D(
        filters=filters,
        kernel_size=(1, 1, 1),
        strides=1,
        data_format=data_format,
        name=f'Res_{name}' if name else None)(inp)

    # # axis=1 for channels_first data format
    # # No. of groups = 8, as given in the paper
    # x = GroupNormalization(
    #     groups=1,
    #     axis=1 if data_format == 'channels_last' else 0,
    #     name=f'GroupNorm_1_{name}' if name else None)(inp)
    # x = Activation('relu', name=f'Relu_1_{name}' if name else None)(x)
    # x = Conv3D(
    #     filters=filters,
    #     kernel_size=(3, 3, 3),
    #     strides=1,
    #     padding='same',
    #     data_format=data_format,
    #     name=f'Conv3D_1_{name}' if name else None)(x)

    x = GroupNormalization(
        groups=1,
        axis=1 if data_format == 'channels_last' else 0,
        name=f'GroupNorm_2_{name}' if name else None)(inp_res)
    x = Activation('relu', name=f'Relu_2_{name}' if name else None)(x)
    x = Conv3D(
        filters=filters,
        kernel_size=(3, 3, 3),
        strides=1,
        padding='same',
        data_format=data_format,
        name=f'Conv3D_2_{name}' if name else None)(x)

    out = Add(name=f'Out_{name}' if name else None)([x, inp_res])
    return out


# From keras-team/keras/blob/master/examples/variational_autoencoder.py
def sampling(args):
    z_mean, z_log_var = args
    batch = tf.shape(z_mean)[0]
    dim = tf.shape(z_mean)[1]
    epsilon = tf.keras.backend.random_normal(shape=(batch, dim))
    return z_mean + tf.exp(0.5 * z_log_var) * epsilon


################################################################################
def loss_d(y_true, y_pred):
    intersection = K.sum(K.abs(y_true * y_pred), axis=-1)
    dn = K.sum(K.square(y_true) + K.square(y_pred), axis=-1) + 1e-8
    return K.mean(2 * intersection / dn, axis=-1)


def loss_gt(e=1e-8):
    """
    loss_gt(e=1e-8)
    ------------------------------------------------------
    Since keras does not allow custom loss functions to have arguments
    other than the true and predicted labels, this function acts as a wrapper
    that allows us to implement the custom loss used in the paper. This function
    only calculates - L<dice> term of the following equation. (i.e. GT Decoder part loss)

    L = - L<dice> + weight_L2 ∗ L<L2> + weight_KL ∗ L<KL>

    Parameters
    ----------
    `e`: Float, optional
        A small epsilon term to add in the denominator to avoid dividing by
        zero and possible gradient explosion.

    Returns
    -------
    loss_gt_(y_true, y_pred): A custom keras loss function
        This function takes as input the predicted and ground labels, uses them
        to calculate the dice loss.

    """

    def loss_gt_(y_true, y_pred):
        intersection = K.sum(K.abs(y_true * y_pred))
        dn = K.sum(K.square(y_true) + K.square(y_pred)) + e

        return - K.mean(2 * intersection / dn)

    return loss_gt_


def loss_VAE(input_shape, z_mean, z_var, weight_L2=0.1, weight_KL=0.1):
    """
    loss_VAE(input_shape, z_mean, z_var, weight_L2=0.1, weight_KL=0.1)
    ------------------------------------------------------
    Since keras does not allow custom loss functions to have arguments
    other than the true and predicted labels, this function acts as a wrapper
    that allows us to implement the custom loss used in the paper. This function
    calculates the following equation, except for -L<dice> term. (i.e. VAE decoder part loss)

    L = - L<dice> + weight_L2 ∗ L<L2> + weight_KL ∗ L<KL>

    Parameters
    ----------
     `input_shape`: A 4-tuple, required
        The shape of an image as the tuple (c, H, W, D), where c is
        the no. of channels; H, W and D is the height, width and depth of the
        input image, respectively.
    `z_mean`: An keras.layers.Layer instance, required
        The vector representing values of mean for the learned distribution
        in the VAE part. Used internally.
    `z_var`: An keras.layers.Layer instance, required
        The vector representing values of variance for the learned distribution
        in the VAE part. Used internally.
    `weight_L2`: A real number, optional
        The weight to be given to the L2 loss term in the loss function. Adjust to get best
        results for your task. Defaults to 0.1.
    `weight_KL`: A real number, optional
        The weight to be given to the KL loss term in the loss function. Adjust to get best
        results for your task. Defaults to 0.1.

    Returns
    -------
    loss_VAE_(y_true, y_pred): A custom keras loss function
        This function takes as input the predicted and ground labels, uses them
        to calculate the L2 and KL loss.

    """

    def loss_VAE_(y_true, y_pred):
        H, W, D, c = input_shape
        n = c * H * W * D

        loss_L2 = K.mean(K.square(y_true - y_pred), axis=(1, 2, 3))  # original axis value is (1,2,3).

        loss_KL = (1 / n) * K.sum(
            K.exp(z_var) + K.square(z_mean) - 1. - z_var,
            axis=-1
        )

        return weight_L2 * loss_L2 + weight_KL * loss_KL

    return loss_VAE_


################################################################################

def build_model(input_shape=(9, 9, 32, 1), output_channels=16, dice_e=1e-8,
                config=None):
    H, W, D, c = input_shape

    # -------------------------------------------------------------------------
    # Encoder
    # -------------------------------------------------------------------------

    # #Input Layer
    # print(input_shape)
    inp = Input(input_shape)

    ## The Initial Block
    x = Conv3D(
        filters=64,
        kernel_size=(3, 3, 3),
        strides=1,
        padding='same',
        data_format='channels_last',
        name='Input_x1')(inp)

    ## Dropout (0.2)
    # x = SpatialDropout3D(rate=0.2)(x)
    x = Dropout(rate=0.2)(x)

    ## Green Block x1 (output filters = 8)
    x1 = green_block(x, 16, name='x1')
    x = Conv3D(
        filters=16,
        kernel_size=(3, 3, 3),
        strides=(3, 3, 2),  # 9 9 32 -> 9 9 16
        padding='same',
        data_format='channels_last',
        name='Enc_DownSample_16')(x1)
    ## Green Block x2 (output filters = 16)
    x = green_block(x, 32, name='Enc_16_1')
    x2 = green_block(x, 32, name='x2')
    x = Conv3D(
        filters=32,
        kernel_size=(3, 3, 3),
        strides=(1, 1, 2),  # 9 9 16 -> 9 9 8
        padding='same',
        data_format='channels_last',
        name='Enc_DownSample_32')(x2)

    ## Green Blocks x2 (output filters = 32)
    x = green_block(x, 64, name='Enc_32_1')
    x3 = green_block(x, 64, name='x3')
    x = Conv3D(
        filters=64,
        kernel_size=(3, 3, 3),
        strides=(1, 1, 2),  # 9 9 8 -> 9 9 4
        padding='same',
        data_format='channels_last',
        name='Enc_DownSample_64')(x3)

    ## Green Blocks x4 (output filters = 64)
    x = green_block(x, 128, name='Enc_64_1')
    x = green_block(x, 128, name='Enc_64_2')
    x = green_block(x, 128, name='Enc_64_3')
    x = green_block(x, 128, name='Enc_64_4')
    x4 = green_block(x, 128, name='x4')

    # -------------------------------------------------------------------------
    # Decoder
    # -------------------------------------------------------------------------

    ## GT (Groud Truth) Part
    # -------------------------------------------------------------------------

    ### Green Block x1 (output filters=32)
    x = Conv3D(
        filters=64,
        kernel_size=(1, 1, 1),
        strides=1,
        data_format='channels_last',
        name='Dec_GT_ReduceDepth_64')(x4)
    x = UpSampling3D(
        size=(1, 1, 2),  # 9 9 4 -> 9 9 8
        data_format='channels_last',
        name='Dec_GT_UpSample_32')(x)
    x = Add(name='Input_Dec_GT_32')([x, x3])
    x = green_block(x, 64, name='Dec_GT_64')

    ### Green Block x1 (output filters=16)
    x = Conv3D(
        filters=32,
        kernel_size=(1, 1, 1),
        strides=1,
        data_format='channels_last',
        name='Dec_GT_ReduceDepth_32')(x)
    x = UpSampling3D(
        size=(1, 1, 2),  # 9 9 8 -> 9 9 16
        data_format='channels_last',
        name='Dec_GT_UpSample_16')(x)
    x = Add(name='Input_Dec_GT_16')([x, x2])
    x = green_block(x, 32, name='Dec_GT_32')

    ### Green Block x1 (output filters=8)
    x = Conv3D(
        filters=16,
        kernel_size=(1, 1, 1),
        strides=1,
        data_format='channels_last',
        name='Dec_GT_ReduceDepth_16')(x)
    x = UpSampling3D(
        size=(3, 3, 2),  # 9 9 16 -> 9 9 32
        data_format='channels_last',
        name='Dec_GT_UpSample_8')(x)
    x = Add(name='Input_Dec_GT_8')([x, x1])
    x = green_block(x, 16, name='Dec_GT_8')

    ### Blue Block x1 (output filters=4)
    x = Conv3D(
        filters=8,
        kernel_size=(3, 3, 3),
        strides=1,
        padding='same',
        data_format='channels_last',
        name='Input_Dec_GT_Output')(x)

    ### Output Block 3D
    out_GT = Conv3D(
        filters=1,
        kernel_size=(1, 1, 1),
        strides=1,
        data_format='channels_last',
        name='Dec_GT_Output')(x)

    ## convolutional layers
    # conv_layer1 = Conv3D(filters=8, kernel_size=(3, 3, 7), activation='relu')(inp)
    # conv_layer2 = Conv3D(filters=16, kernel_size=(3, 3, 5), activation='relu')(conv_layer1)
    # conv_layer3 = Conv3D(filters=32, kernel_size=(3, 3, 3), activation='relu')(conv_layer2)
    # conv3d_shape = conv_layer3.shape
    # conv_layer3 = Reshape((conv3d_shape[1], conv3d_shape[2], conv3d_shape[3] * conv3d_shape[4]))(conv_layer3)
    # conv_layer4 = Conv2D(filters=64, kernel_size=(3, 3), activation='relu')(conv_layer3)
    #
    # flatten_layer = Flatten()(conv_layer4)
    #
    # ## fully connected layers
    # dense_layer1 = Dense(units=256, activation='relu')(flatten_layer)
    # dense_layer1 = Dropout(0.4)(dense_layer1)
    # dense_layer2 = Dense(units=128, activation='relu')(dense_layer1)
    # dense_layer2 = Dropout(0.4)(dense_layer2)
    # out_AE_YSA = Dense(units=output_channels, activation='softmax')(dense_layer2)

    # out_GT_shape = out_GT._keras_shape
    # out_GT_reshape = Reshape((out_GT_shape[1], out_GT_shape[2], out_GT_shape[3] * out_GT_shape[4]))(out_GT)
    #
    # conv2d_layer = Conv2D(filters=256, kernel_size=(1, 1), activation='relu')(out_GT_reshape)
    #
    # flatten_layer = Flatten()(conv2d_layer)
    # dense_layer1 = Dense(units=512, activation='relu')(flatten_layer)
    # dense_layer1 = Dropout(rate=0.4)(dense_layer1)
    # dense_layer2 = Dense(units=128, activation='relu')(dense_layer1)
    # # dense_layer2 = Dropout(rate=0.4)(dense_layer2)
    # out_AE_YSA = Dense(units=output_channels, activation='softmax', name='Output')(dense_layer2)

    ## VAE (Variational Auto Encoder) Part
    # -------------------------------------------------------------------------

    ### VD Block (Reducing dimensionality of the data)
    x = GroupNormalization(groups=1, axis=1, name='Dec_VAE_VD_GN')(x4)
    x = Activation('relu', name='Dec_VAE_VD_relu')(x)
    x = Conv3D(
        filters=16,
        kernel_size=(3, 3, 3),
        strides=(1, 1, 2),  # 9 9 4 -> 3 3 4
        padding='same',
        data_format='channels_last',
        name='Dec_VAE_VD_Conv3D')(x)

    # Not mentioned in the paper, but the author used a Flattening layer here.
    x = Flatten(name='Dec_VAE_VD_Flatten')(x)
    x = Dense(144, name='Dec_VAE_VD_Dense')(x)  # 3*3*4

    ### VDraw Block (Sampling)
    z_mean = Dense(72, name='Dec_VAE_VDraw_Mean')(x)
    z_var = Dense(72, name='Dec_VAE_VDraw_Var')(x)
    x = Lambda(sampling, name='Dec_VAE_VDraw_Sampling')([z_mean, z_var])

    ### VU Block (Upsizing back to a depth of 256)
    x = Dense((H // 1) * (W // 1) * (D // 16) * (c // 1))(x)
    x = Activation('relu')(x)
    x = Reshape(((H // 1), (W // 1), (D // 16), (c // 1)))(x)
    x = Conv3D(
        filters=64,
        kernel_size=(1, 1, 1),
        strides=1,
        data_format='channels_last',
        name='Dec_VAE_ReduceDepth_64')(x)
    x = UpSampling3D(
        size=(1, 1, 2),  # 9 9 4
        data_format='channels_last',
        name='Dec_VAE_UpSample_64')(x)

    ### Green Block x1 (output filters=128)
    x = Conv3D(
        filters=32,
        kernel_size=(1, 1, 1),
        strides=1,
        data_format='channels_last',
        name='Dec_VAE_ReduceDepth_32')(x)
    x = UpSampling3D(
        size=(1, 1, 2),  # 9 9 4 -> 9 9 8
        data_format='channels_last',
        name='Dec_VAE_UpSample_32')(x)
    x = green_block(x, 32, name='Dec_VAE_32')

    ### Green Block x1 (output filters=64)
    x = Conv3D(
        filters=16,
        kernel_size=(1, 1, 1),
        strides=1,
        data_format='channels_last',
        name='Dec_VAE_ReduceDepth_16')(x)
    x = UpSampling3D(
        size=(1, 1, 2),  # 9 9 8 -> 9 9 16
        data_format='channels_last',
        name='Dec_VAE_UpSample_16')(x)
    x = green_block(x, 16, name='Dec_VAE_16')

    ### Green Block x1 (output filters=32)
    x = Conv3D(
        filters=8,
        kernel_size=(1, 1, 1),
        strides=1,
        data_format='channels_last',
        name='Dec_VAE_ReduceDepth_8')(x)
    x = UpSampling3D(
        size=(1, 1, 2),  # 9 9 16 -> 9 9 32
        data_format='channels_last',
        name='Dec_VAE_UpSample_8')(x)
    x = green_block(x, 8, name='Dec_VAE_8')

    ### Blue Block x1 (output filters=32)
    x = Conv3D(
        filters=4,
        kernel_size=(3, 3, 3),
        strides=1,
        padding='same',
        data_format='channels_last',
        name='Input_Dec_VAE_Output')(x)

    ### Output Block
    out_VAE = Conv3D(
        filters=1,
        kernel_size=(1, 1, 1),
        strides=1,
        data_format='channels_last',
        name='VAE_output')(x)

    conv_layer1 = Conv3D(filters=8, kernel_size=(3, 3, 7), activation='relu')(out_VAE)
    conv_layer2 = Conv3D(filters=16, kernel_size=(3, 3, 5), activation='relu')(conv_layer1)
    conv_layer3 = Conv3D(filters=32, kernel_size=(3, 3, 3), activation='relu')(conv_layer2)
    conv3d_shape = conv_layer3.shape
    conv_layer3 = Reshape((conv3d_shape[1], conv3d_shape[2], conv3d_shape[3] * conv3d_shape[4]))(conv_layer3)
    conv_layer4 = Conv2D(filters=64, kernel_size=(3, 3), activation='relu')(conv_layer3)

    flatten_layer = Flatten()(conv_layer4)

    ## fully connected layers
    dense_layer1 = Dense(units=256, activation='relu')(flatten_layer)
    dense_layer1 = Dropout(0.4)(dense_layer1)
    dense_layer2 = Dense(units=128, activation='relu')(dense_layer1)
    dense_layer2 = Dropout(0.4)(dense_layer2)
    out_AE_YSA = Dense(units=output_channels, activation='softmax')(dense_layer2)

    # Build and Compile the model
    model = Model(inp, outputs=out_AE_YSA)  # Create the model
    optimization = opti.getOptimizationAlgorithm(config)

    model.compile(
        optimizer=optimization,
        loss=
        'categorical_crossentropy',
        metrics=['accuracy']
    )
    # model.compile(optimizer='adam',
    #               loss='categorical_crossentropy',
    #               metrics=['accuracy, dice_coefficient'])

    # model.compile(
    #    optimizer=adam(lr=1e-5, decay=1e-06),
    #    loss=loss(input_shape, inp, out_VAE, z_mean, z_var, weight_L2=weight_L2, weight_KL=weight_KL),
    #    metrics=['accuracy']
    # )

    return model
