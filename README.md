# A Hybrid Approach with VAE and CNN in the Classification of Hyperspectral Satellite Images


[![made-with-Markdown](https://img.shields.io/badge/Made%20with-Markdown-1f425f.svg)](http://commonmark.org) 
[![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)](http://perso.crans.org/besson/LICENSE.html)
[![Generic badge](https://img.shields.io/badge/Tensorflow-2.1.0-F88921.svg)](https://shields.io/) 
[![Generic badge](https://img.shields.io/badge/Keras-2.3.1-EC2F2F.svg)](https://shields.io/) 
[![Generic badge](https://img.shields.io/badge/Python-3.7.9-1E96C0.svg)](https://shields.io/) 


##  Requirements

| Minimum | Recommended |
| ------ | ------ |
| nvidia-smi: 410  | nvidia-smi: 450  |
| CUDA Version: 10.2 | CUDA Version: 11.0 |
| Cudnn: 7.5 | Cudnn: 8.0 |


## How to run

First of all check the [dataset](https://gitlab.com/mcanv/hsi-tez/-/blob/master/docs/datasetInfo.md) for [config](https://gitlab.com/mcanv/hsi-topic/-/blob/master/config.yml) settings

1. `git clone https://gitlab.com/mcanv/hsi-topic.git`
2. `cd hsi-tez`
3. `export PYTHONPATH="$PWD"`
4. `pip install virtualenv`
5. `virtualenv env`
6. `source env/bin/activate`
7. `pip install -r requirements.txt`
8. `python3 Core/main/main.py`

# Results of The Models

|Dataset Name|Model Id|Kappa(%)|Overall Accuracy|Average Accuracy|Date      |
|------------|--------|--------|----------------|----------------|----------|
|BW          |4392138 |0.996   |99.648          |99.648          |2020-12-22|
|IP          |4611239 |0.989   |99.038          |99.038          |2020-12-22|
|PC          |3467128 |0.998   |99.874          |99.874          |2020-12-22|
|KSC         |6591454 |0.868   |88.130          |88.130          |2020-12-22|
|SA          |8769185 |0.999   |99.881          |99.881          |2020-12-22|
|UP          |3467128 |0.992   |99.402          |99.402          |2020-12-22|



# Ground vs Predected images

## Indian Pines

<img src="docs/imgs/IP_ground_.png"  width="200">        <img src="docs/imgs/IP_predicted_.png"  width="200">

## Salians 

<img src="docs/imgs/SA_ground_.png"  width="200">        <img src="docs/imgs/SA_predicted_.png"  width="200">


## Universtiy of Pavia

<img src="docs/imgs/UP_ground_.png"  width="200">        <img src="docs/imgs/UP_predicted_.png"  width="200">

## Pavia Center

<img src="docs/imgs/PC_ground_.png"  width="200">        <img src="docs/imgs/PC_predicted_.png"  width="200">

## Botswana Center

<img src="docs/imgs/BW_ground_.png"  width="200">        <img src="docs/imgs/BW_predicted_.png"  width="200">

## Kennedy Space Center Center

<img src="docs/imgs/KSC_ground_.png"  width="200">        <img src="docs/imgs/KSC_predicted_.png"  width="200">


## TODO

- [x] Readme will be readable
- [ ] config will be more comprehensive
- [x] Other datasets will be load
- [x] Visualization of the results will be done
- [x] The result will create csv format **dataset | modelId | OA | AA | Kappa**
- [ ] Training time will save as log
- [x] GPU Memory Fraction will be add limitation for low systems
- [ ] Different networks will be designed
- [x] Files will be added to **train** and **test** in _Jupyter_ and _Google-Colab_


# Coding & Supervisor
<table>
  <tr>
    <td align="center"><a href="https://github.com/mcvarer"><img src="https://avatars0.githubusercontent.com/u/20646732?s=460&u=3f5a5a9806646a4b0d0cc9f5da2e3cad3acd292b&v=4" width="200px;" alt=""/><br /><sub><center><b>Murat Can VARER</b></a> </td>

</td>
 <td align="center"><a href="https://ceng2.ktu.edu.tr/~aykut/"><img src="https://ceng2.ktu.edu.tr/~aykut/images/myphoto.jpg"  width="250px;" alt=""/><br /><sub><center><b>Murat AYKUT</b></a> </td>
</td>
  </tr>
</table>
