## Hiperspektral Uydu Görüntülerinin Sınıflandırmasında VAE ve CNN ile Hibrit Bir Yaklaşım

* * *
### Danışman: **Dr.Öğr.Üyesi Murat AYKUT**
* * *


### Tezin Amacı

Yüksek hesaplamaların daha hızlı yapıldığı günümüz teknolojisinde artık işlem yükü fazla olan
projeler daha kolaylıkla yapılmaktadır. Evrişimli sinir ağları (ESA) (Convolution Neural Networks (CNN) ) görüntüler için sınıflandırma problemlerini çözmek amaçlı geliştirilmiş bir Derin öğrenme modelidir. Derin öğrenme, öğrenme düzeylerini temsil eden makine öğrenmesinin dalını ifade eder yani burada bahsi geçen derin kelimesi, sınıflandırılacak görüntülerin özelliklerini sinir ağı yapısının derinliğinden alır. Uzaktan algılama, dünyanın yüzeyi hakkında bilgi edinme bilimidir. Günümüzde uzaktan algılama başta askeri hava sahası olmak üzere deprem hazılığı aşamasında toplanma bölgelerinin de belirlenmesinde önemli rol oynamaktadır.  Bu tez çalışmasında uydudan çekilmiş Hiperspektral  Görüntüleri (Hiperspectral Images (HSI) ) CNN modelini kullanarak sınıflandırılması yapılacaktır.
* * *
### İlgili Alana katkısı Özgün Değerleri ve Yöntemi


Daha önce yapılan çalışmalara bakıldığında, Uzaktan algılama verilerini kullanan hava sahnesi sınıflandırılması, verilerin özelliklerinden dolayı (az sayıda etiketli veri bulunması ve yüksek boyutluluk) hem askeri hem de sivil alanlarda en zorlu araştırma alanlarından biridir [1] Literatürde önceki yöntemlerle karşılaştırıldığında, derin öğrenme temelli spektral-uzamsal sınıflandırma stratejileri HSI görüntüleri için daha uygundur ve sınıflandırma doğruluğunun iyileştirilmesini sağlamaktadır. Spektral ve uzamsal bilgiyi birlestirmek icin iki ana strateji vardir; bunlardan biri önce spektral ve uzamsal özellikleri ayıklamak, sonra da spektral ve uzamsal özellikleri birleştirmek; diğeri ise doğrudan HSI'nin 3D alt küplerinden derin spektral-uzamsal özellikleri çıkmaktadır. İlki genellikle 1D-CNN, 2D-CNN benimser, ikincisi tipik olarak 3D-CNN kullanır. [2] çalışmada, spektral ve uzamsal özellikleri çıkartmak için sırasıyla 1D-CNN ve 2D-CNN kullanılmıştır. 

HSI'lerin sınırlı eğitim örneklerinin neden olduğu aşırı öğrenme probleminin üstesinden gelmek için, elde edilmiş görüntler üzerinde çeşitli işlemler (çevirme ve döndürme gibi) ile verilerin arttırılması sağlanmıştır. 3D HSI nedeniyle 2D-CNN'in karşılaştığı en büyük zorluk ek boyuttur. Literatürdeki CNN tabanlı spektral-uzamsal sınıflandırma yöntemleri, 2D-CNN'yi 3D-CNN'ye genişleterek veya 3D HSI'yi 2D HSI'ye yeniden düzenleyerek bu zorlukla başa çıkmaktadır. Örneğin [3] ve [4] çalışmalarda, spektral-uzamsal özellikleri öğrenmek için 2D-CNN kullanılmıştır.

Literatürü taradığımızda yukarıda da belirttiğim gibi 2D-CNN ve 3D-CNN gibi bir çok yöntemlerle projeler yapılmıştır. Tezde derin öğrenmenin hızla geliştiği yeni sinir aği modellerinin hızla arttığı bu dönem popüler ağ mimarilarinden olan Varyasyonel Oto Kodlayıcı (Variational Auto-Encoder (VAE) ) ile ortak kullanımı ile literatüre yeni bir katkı bakış açısı katmayı planlamaktadır. Bu tezde CNN modeli bize spektral görüntülerin özelliklerini çıkarmamızı sağlar iken VAE modeli ise bu aşamada çıkarılan bu özelliklerin gaussian dağılımı ile farklı özellikler çıkararak çeşitliliği sağlayarak modelin gürültülerden artılırmasını sağlamaktır. Bu şekilde bu iki modelin birlikte kullanımı sınıflandırmaya etkisinin ne olacağı incelenecektir.
* * *
### Veri setleri

Veri Kümeleri ve Performans Ölçümü Sistemin performansı şu benchmark veri seti üzerinden değerlendirilecektir:

1. **Indian Pines**: 145x145 uzamsal çözünürlük ve 224 spektral bant
2. **Salinas**: 512x217 uzamsal çözünürlük ve 224 spektral bant
3. **Kennedy Uzay Merkezi**: 512x614 uzamsal çözünürlük ve 224 spektral bant
4. **Pavia Center**: 1096x492 uzamsal çözünürlük ve 115 spektral bant
5. **Pavia Üniversitesi**: 610x340 uzamsal çözünürlük ve 115 spektral bant
Indian Pines, Uzay Merkezi veri setlerindeki görüntüler AVIRIS havadan görülebilen / kızılötesi görüntüleme spektrometresi cihazı ile toplanmaktadır.

Hava sahnesinde sınıflandırmada, yöntemin performansını ölçmek için esas olarak iki performans ölçütü kullanılacaktır: 

- Genel doğruluk (doğru olarak sınıflandırılmış örneklerin sayısı / tüm örneklerin sayısı) ve 
- Gerçek pozitif –TP (tahmin edilen ve gerçek pozitif), gerçek negatif - TN (tahmin edilen ve gerçek negatif), yanlış pozitif - FP (tahmin edilen pozitif, gerçek negatif) ve yanlış negatif - FN (tahmin edilen negatif, gerçek pozitif) oranlarından oluşan tahmini hata matrisidir (confusion matrix). 

* * *

### Referanslar

1. Deep learning for remote sensing image classification: A survey
2. Zhang, H., Li, Y., Zhang, Y., & Shen, Q. (2017). Spectralspatial classification of hyperspectral imagery using a dualchannel convolutional neural network. Remote Sensing Letters, 8(5), 438Œ447
3. Chen, Y., Jiang, H., Li, C., Jia, X., & Ghamisi, P. (2016). Deep feature extraction and classification of hyperspectral images based on convolutional neural networks. IEEE Transactions on Geoscience and Remote Sensing, 54(10), 6232Œ6251
4. Li, Y., Zhang, H., & Shen, Q. (2017). Spectral-spatial classification of hyperspectral imagery with 3d convolutional neural network. Remote Sensing, 9(1), 67


